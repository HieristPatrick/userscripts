// ==UserScript==
// @name         pr0game Space Background
// @namespace    https://pr0game.pebkac.me/
// @version      0.1
// @description  Adds a nice background image to the game.
// @author       AxelFLOSS
// @match        https://pr0game.com/*
// @updateURL    https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_space_background.user.js
// @downloadURL  https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_space_background.user.js
// @supportURL   https://codeberg.org/pr0game/userscripts/issues
// @icon         https://pr0game.com/favicon.ico
// @grant        GM_addStyle
// ==/UserScript==

(function() {
    'use strict';

    GM_addStyle(`
        html {
            animation: movingbg 10s linear infinite;
        }
        @media only screen and (max-width: 640px) {
            html {
                background-image: url("https://www.linkpicture.com/q/astronomy-3187445_640.jpg");
            }
        }
        @media only screen and (min-width: 641px) and (max-width: 1280px) {
            html {
                background-image: url("https://www.linkpicture.com/q/astronomy-3187445_1280.jpg");
            }
        }
        @media only screen and (min-width: 1281px) {
            html {
                background-image: url("https://www.linkpicture.com/q/astronomy-3187445_1920.jpg");
            }
        }
        @keyframes movingbg {
            from {
                background-size: 102%;
            }
            50% {
                background-size: 102.5%;
            }
            to {
                background-size: 102%;
            }
        }
    `);
})();
